import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { StorageService } from '../service/storage.service';

import { Observable } from 'rxjs';
import { catchError, timeout, retry } from 'rxjs/operators';

@Injectable()
export class UploadService {
  private handleError: HandleError;
  
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler, private storageService: StorageService) {
    this.handleError = httpErrorHandler.createHandleError('UploadService');
  }

  /* Content Type undefined used */
  private httpOptions;

  uploadImage(url,data){
    if (this.storageService.getItem('user')!=null && this.storageService.getItem('user').data.accessToken)
    {
      this.httpOptions = {
        headers: new HttpHeaders({
          "X-API-KEY": "MMGATPL",
          "Authorization": 'Bearer'+ this.storageService.getItem('user').data.accessToken
        })
      };
    }
    else
    {
      this.httpOptions = {
        headers: new HttpHeaders({
          "X-API-KEY": "MMGATPL"
        })
      };
    }
    return this.http.post(url, data, this.httpOptions)
    .pipe(
      
      
      catchError(this.handleError('uploadImage', []))
    );
  }

}
