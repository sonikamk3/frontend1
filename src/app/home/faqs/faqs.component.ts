import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {

  faqTemplate = [
    {
      'title': 'Driver FAQs:', 'data': [
        {
          'question': 'When do I get paid?', 'answers': [
            { 'ans': 'All payments will be sent through direct deposit/ Bank Transfer.' },
            { 'ans': 'It will either weekly or monthly payment* depending on MMG driver\'s agreement.' }
          ]
        },
        {
          'question': 'How do tolls work for MMG drivers?', 'answers': [
            { 'ans': 'Toll/Highway charges to be borne by Driver.' }
          ]
        },
        {
          'question': 'What are the steps to join MMG?', 'answers': [
            { 'ans': 'The first step is to apply online.' },
            { 'ans': 'The online application form only takes a few minutes. Your application will be screened for basic compatibility.' },
            { 'ans': 'Within 30 minutes, you will receive a response from our team.' },
            { 'ans': 'If your application has been approved, you will be invited to our nearest franchise/office for verification with documents.' },
            { 'ans': 'To start Trips/booking on the MMG platform, you will need to pass a vehicle inspection and background check.' },
            { 'ans': 'You will be able to upload your vehicle documents, including vehicle registration and insurance, as well as proof of a safety inspection by Field Officer.' },
            { 'ans': 'Once verified, we will request your bank information to set up direct deposit payments. Then you will be activated on the MMG.' }
          ]
        },
        {
          'question': 'I tried to log into my account, but it says it is blocked by admin. What does this mean?', 'answers': [
            { 'ans': 'After you apply to join the MMG platform, we will review your application and email you with the next steps.' },
            { 'ans': 'Your account will remain blocked and your status inactive until you complete the entire onboarding process.' },
          ]
        },
        {
          'question': 'What documents do I need to provide during my application?', 'answers': [
            { 'ans': 'MMG will need to verify your vehicle insurance and registration.' },
            { 'ans': 'For more information about what these documents are, and how to find them, click here.' }
          ]
        },
        {
          'question': 'Can a customer ride in my vehicle?', 'answers': [
            { 'ans': 'No, customers should never ride in your vehicle.' }
          ]
        },
        {
          'question': 'What happens if I need to cancel a booking?', 'answers': [
            { 'ans': 'If a delivery professional accepts a booking and cancels or does not complete the project as scheduled, MMG may at its discretion impose penalties. the penalty charges will increase as the number of cancelations increases.' }
          ]
        },
        {
          'question': 'How does the bidding process work?', 'answers': [
            { 'ans': 'The bidding process works by having a network of drivers that compete with bids on a customer\'s booking.' },
            { 'ans': 'Based on the price you bid and your driver profile, the customer will have an option to book an MMG trip with which driver they want to go with.' },
            { 'ans': 'Once a bid is accepted, the auction will end and no more bids can be placed from there on.' }
          ]
        },
        {
          'question': 'What happens if no-one is there to receive the goods?', 'answers': [
            { 'ans': 'Please return to the nearest MMG franchise/warehouse.' }
          ]
        },
        {
          'question': 'My app keeps crashing or freezing. What do I do?', 'answers': [
            { 'ans': 'Step 1 - Make sure that there aren’t any updates available for the app.' },
            { 'ans': 'Step 2 - Try force closing the app and opening it again.' },
            { 'ans': 'Step 3 - Try uninstalling then reinstalling the app.' }
          ]
        },
        {
          'question': 'How can I change my e-mail/phone number address?', 'answers': [
            { 'ans': 'You can change your e-mail address in the Edit Profile section.' }
          ]
        }
      ]
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
