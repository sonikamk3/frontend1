import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Router } from '@angular/router';
import { PopupGeneralComponent } from 'src/app/popup-general/popup-general.component';
import { StorageService } from 'src/app/service/storage.service';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: any;
  id: any;
  firstName:any;

  constructor(private employeeService: EmployeeService,private dialog: MatDialog, private storageService: StorageService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

private reloadData() {
 let response;
 response =  this.employeeService.getEmployeesList();
 response.subscribe((data)=>{
  this.employees={...data};
  this.employees = this.employees.data;
  console.log(this.employees);
  });
  }

  deleteEmployee(id: string,firstName:string) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title : 'Are You Sure Want To Delete ' +"\n " +"id" + id  +"firstName " + firstName  
    }

    const dialogRef = this.dialog.open(PopupGeneralComponent,
      dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;
          this.employeeService.deleteEmployee(id)
          .subscribe(data => {
            this.reloadData();
          },
          error => console.log(error));
     //this.notificationService.deleteToken();
        // this.storageService.clearStorage();
        // this.router.navigate(['/']).then(() => {
        //   window.location.reload();
        // });
    });
    // this.employeeService.deleteEmployee(id)
    //   .subscribe(
    //     data => {
    //       // console.log(data);
    //       // alert("Are you sure ?");
    //       this.reloadData();
    //       // alert("data is deleted sucessfully");
    //     },
    //     error => console.log(error));
  }

  employeeDetails(id: string){
    this.router.navigate(['details', id]);
  }

  updateEmployee(id: string){

    this.router.navigate(['update', id]);
  }
}
