import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-update-employee',
  templateUrl: './update-employee.component.html',
  styleUrls: ['./update-employee.component.css']
})
export class UpdateEmployeeComponent implements OnInit {

  id: string;
  employee: any;
  firstName: any;
  lastName: any;
  gender: any;
  address:any;

  constructor(private route: ActivatedRoute,private router: Router,
    private employeeService: EmployeeService) { }

    ngOnInit() {
      this.employee = new Employee();
  
      this.id = this.route.snapshot.params['id'];
      
      this.employeeService.getEmployee(this.id)
        .subscribe(data => {
          console.log(data)
          this.employee ={...data};
          this.employee = this.employee.data;
        }, error => console.log(error));
    }
  
    updateEmployee(firstName,lastName,gender,address) {
      let updatedata;
      updatedata={
      id : this.id,
      firstName:firstName,
      lastName:lastName,
      gender:gender,
      address:address,
    }
      this.employeeService.updateEmployee(updatedata)
        .subscribe(data => {
          console.log(data);
         alert("Updated Successfully");
          this.gotoList();
        }, error => console.log(error));
    }
  
    gotoList() {
      this.router.navigate(['/employees']);
    }
  }
  
