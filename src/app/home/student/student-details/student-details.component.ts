import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {
  employee: any;
  id: string;

  constructor(private route: ActivatedRoute,private router: Router,
    private studentService:StudentService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    
    
    this.studentService.getEmployee(this.id)
      .subscribe(data => {
        console.log(data)
        this.employee ={...data};
        this.employee=this.employee.data;
        
      }, error => console.log(error));

  }
  list(){
    this.router.navigate(['employees1']);
  }
}
