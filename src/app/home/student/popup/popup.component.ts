import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Globalservice } from 'src/app/service/globals.service';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  public title: String;
  public firstName: any;
  public id:any;
  studentform: FormGroup;
  employeeData: any;
  employees: any;
  FormGroup: any;

  constructor(private formBuilder: FormBuilder, private globalService: Globalservice, private studentService: StudentService,private dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any) {
      this.studentform = this.formBuilder.group({
      
        firstName: new FormControl('', [Validators.required]),
        id:new FormControl('',[Validators.required]),
      });
     }
  ngOnInit() //ngInit is used to display 
  {
    this.employeeData = this.globalService.getEmployeeid();
    console.log(this.employeeData);
    if (this.employeeData) {
      this.studentService. getEmployee(this.employeeData)
        .subscribe(data => {
          this.employees = { ...data };//3 dots are used to fetch only values insted of all object
          console.log(data)
          this.employees = this.employees.data;
           console.log(data)

          this.employees.id;
          this.employees.firstName;
          delete  this.employees.lastName; 
          delete  this.employees.gender;
          delete  this.employees.status; 
          delete  this.employees.address;

  
          console.log(this.employees);
          this.studentform.setValue(this.employees);
         this.FormGroup.disable()
        });
        
    }

   
   
    if(this.dialogData){
      this.title = this.dialogData.title;
      this.firstName=this.dialogData.firstName;
      this.id=this.dialogData.id;
    }
    else
      this.dialogRef.close();
  }
  disable(){
    this.disable();
  }

  close()
  {
    this.dialogRef.close();
  }

  save()
  {
    this.dialogRef.close(this.title);
  }
}
