import { UpdateEmployeeComponent } from './../../employees/update-employee/update-employee.component';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PopupGeneralComponent } from 'src/app/popup-general/popup-general.component';
import { MatDialogRef } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { StudentService } from './../student.service';
import { Globalservice } from 'src/app/service/globals.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { PopupComponent } from '../popup/popup.component';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-student-update',
  templateUrl: './student-update.component.html',
  styleUrls: ['./student-update.component.css']
})
export class StudentUpdateComponent implements OnInit {
  // baseUrl = 'http://localhost:8085/demo/api/v1/employee';
  //  private headers ={ "X-API-KEY": "MMGATPL",'content-type':'application/json','Access-Control-Allow-Headers':'*'}; 
  studentform: FormGroup;
  employeeData: any;
  employees: any;
  favoriteSeason: string;
  seasons: string[] = ['Male', 'Female'];
  errorMsgs = AppSettings.ERROR_MSGS;
  constructor(private formBuilder: FormBuilder, private globalService: Globalservice, private studentService: StudentService, private router: Router, private http: HttpClient, private dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any) {
    this.studentform = this.formBuilder.group({
       id:new FormControl('',[Validators.required]),
      firstName: new FormControl('', [Validators.required, Validators.minLength(2),Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(1),Validators.pattern('[a-zA-Z ]*'),Validators.maxLength(50)]),
      gender:new FormControl('',[Validators.required]),
      phoneNumber:new FormControl('',[Validators.required, Validators.pattern('[6-9][0-9]{9}]*')]),
      email:new FormControl('',[Validators.required]),
      address:new FormControl('',[Validators.required,Validators.minLength(2),Validators.maxLength(250)]),
      panCardNo:new FormControl('',[Validators.required,Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}]*')]),
      
    });
  }
  ngOnInit() {
    this.employeeData = this.globalService.getEmployeeid();
    console.log(this.employeeData);
  
      this.studentService.getEmployee(this.employeeData)
        .subscribe(data => {
          console.log(data) //console.log is used to print the errors in console
          this.employees = { ...data };//3 dots are used to fetch only values insted of object
          console.log(data)
          this.employees = this.employees.data;
           console.log(data)

          //  this.employees.id;
          //  this.employees.firstName;
          //  this.employees.lastName;
          //  this.employees.gender;
          //  this.employees.phoneNumber;
          //  this.employees.email;   
          // this.employees.address;
          // this.employees.panCardNumber;
          delete  this.employees.status; 
          console.log(this.employees);
          this.studentform.setValue(this.employees);
        });
    

  }
  updateEmployee() {
    let data = this.studentform.value;
    console.log(this.studentform)
    if (this.studentform.invalid) {
      return;
  }
    this.studentService.updateEmployee(data).subscribe((Response) => {
      alert("updated succesfully")
      this.dialogRef.close();
    });
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.studentform.controls[controlName].hasError(errorName);
  }
  close() {
    this.dialogRef.close();
  }
}
