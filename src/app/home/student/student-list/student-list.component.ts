import { StudentAddComponent } from './../student-add/student-add.component';
import { element } from 'protractor';
import { StudentUpdateComponent } from './../student-update/student-update.component';
import { Globalservice } from 'src/app/service/globals.service';
// import { PopupComponent } from './../popup/popup.component';
import { MatDialogConfig } from '@angular/material/dialog';
import { Employee } from './../../employees/employee';
import { StudentService } from './../student.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { PopupComponent } from '../popup/popup.component';
import { StorageService } from 'src/app/service/storage.service';
import { PopupGeneralComponent } from 'src/app/popup-general/popup-general.component';

interface key {
  value: string;
  viewValue: string;
  searchword1: any;
}
@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  title: String;
  id: string;
  firstName: string;


  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'gender', 'address', 'action',];//this is used
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  employees: any;
  private baseUrl = 'http://localhost:8085/demo/api/v1/employee/';
  private baseUrl2 = 'http://localhost:8085/demo/api/v1/employee/status/';
  grid: any;


  transform(employees: any, searchText: any): any {

  }
  keys: string[] = ['firstName', 'lastName', 'gender', 'phoneNumber', 'address'];

  constructor(private studentService: StudentService, private globalService: Globalservice, private dialog: MatDialog, private storageService: StorageService,
    private router: Router) { }

  ngOnInit() {
    this.reload();
  }
  private reload() {
    let response;

    response = this.studentService.getAllEmployee(this.baseUrl);

    response.subscribe((data) => {
      console.log(data);
      this.employees = { ...data };
      this.employees = this.employees.data;
      console.log(this.employees);
      this.pagination();
    });
  }


  deleteEmployee(id: string, firstName: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are You Sure Want To Delete ?',
      id: id,
      firstName: firstName,
    }

    const dialogRef = this.dialog.open(PopupComponent,
      dialogConfig);
    this.globalService.setEmployeeid(id);



    dialogRef.afterClosed().subscribe(
      data => {
        if (data == undefined)
          return;

        this.studentService.deleteEmployee(id,)

          .subscribe(data => {
            this.reload();
            alert("data is deleted sucessfully");
          },
            error => console.log(error));

      });

  }
 

  updateEmployee(id: string, firstName: string) {
 
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(StudentUpdateComponent,dialogConfig);
    this.globalService.setEmployeeid(id);

    dialogRef.afterClosed().subscribe(result => {
      console.log("update");
      this.reload();
    });


  }
  private pagination() {
    this.employees = new MatTableDataSource<any>(this.employees);
    this.employees.paginator = this.paginator;
    this.employees.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.employees.filter = filterValue.trim().toLowerCase();
    if (this.employees.paginator) {
      this.employees.paginator.firstPage();

    }
  }

  employeeDetails(id: string) {
    this.router.navigate(['details1', id]);
  }
  isHide: boolean = true;

  active(element) {


    if (element == true) {
      this.isHide = false;
    }
    else {
      this.isHide = true;
    }
    let response;
    response = this.studentService.getAllEmployee(this.baseUrl2 + "/" + element);
    response.subscribe((data) => {
      this.employees = { ...data };
      this.employees = this.employees.data;
      console.log(this.employees);
      this.pagination();
    });
  }
  get() {

    let response;
    this.isHide = true;

    response = this.studentService.getAllEmployee(this.baseUrl);
    response.subscribe((data) => {
      this.employees = { ...data };
      this.employees = this.employees.data;
      console.log(this.employees);
      this.pagination();
    });

  }
  status: boolean;
  patchEmployee(id: string, status: boolean) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Are You Sure Want To Active or deactive ?',
    }

    const dialogRef = this.dialog.open(PopupGeneralComponent,
      dialogConfig);
    console.log(this.status);
    dialogRef.afterClosed().subscribe(
      data => {
        // console.log(this.status);
        // if (data == undefined)
        // return;
        // console.log(this.status);
        if (status == true) {
          status = false;
        }
        else {
          (status = true)
          // status  == !status;
        }
        console.log(status);
        //  this.globalservice.setEmployeeid(status);


        this.studentService.patchEmployee(id, status)
          .subscribe(data => {
            // this.reload();
            alert(" status updated sucessfully");
          },
            error => console.log(error));

      });
    dialogRef.afterClosed().subscribe(result => {

      // this.reload();
    });
  }
  addemployee1(id: string, firstName: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(StudentAddComponent,dialogConfig);
    
    // this.globalService.setEmployeeid(id);

    dialogRef.afterClosed().subscribe(result => {
      console.log("");
      this.reload();
    });
  }

  searchEmployee(key: string, value: string) {
    console.log(key);
    console.log(value);
    if (value.length == 0) {
      this.reload();
    }
    else {
      this.studentService.searchEmployee(key, value)

        .subscribe(data => {
          this.employees = { ...data };
          this.employees = this.employees.data;
        });

    }
  }
}
