import { UpdateEmployeeComponent } from './../employees/update-employee/update-employee.component';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

   private baseUrl = 'http://localhost:8085/demo/api/v1/employee/';
  private headers ={ "X-API-KEY": "MMGATPL",'content-type':'application/json','Access-Control-Allow-Headers':'*'}; 
  

  constructor(private http: HttpClient) { }

  createEmployee(employee: Object): Observable<Object> {
    console.log(employee);
    return this.http.post(`${this.baseUrl}`, employee);
  }
  getAllEmployee(url){
    return this.http.get(url);
  }
  deleteEmployee(id:string) :Observable<any>{
    return this.http.delete(`${this.baseUrl}/${id}`,{'headers':this.headers,responseType:'text'} );
  }
  getEmployee(id:string):Observable<any>{
    console.log(id);
    return this.http.get(`${this.baseUrl}/${id}`);
  }
  updateEmployee(data):Observable<Object>{
    return this.http.put(`${this.baseUrl}`,data,{'headers':this.headers} );
  }

 patchEmployee(id:string,status:Boolean):Observable<any>{
    return this.http.patch(`${this.baseUrl}/${id}/${status}`,{'headers':this.headers});
 } 
 searchEmployee(key:string,value:string): Observable<any>{
   return this.http.get(`${this.baseUrl}search/${key}/${value}`);
 }

}
