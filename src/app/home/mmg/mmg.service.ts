import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';

import { Observable } from 'rxjs';
import { catchError, map, retry, timeout } from 'rxjs/operators';

@Injectable()
export class MmgService {
  private handleError: HandleError;
  
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('MmgService');
  }

  getGoodsType(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      
      
      catchError(this.handleError('getGoodsType', []))
    );
  }

  getWeights(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      
      
      catchError(this.handleError('getWeights', []))
    );
  }
  getDay(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getDay', []))
    );
  }
  getDates(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      catchError(this.handleError('getDates', []))
    );
  }
  
  
  getVehicles(url){
    return this.http.get(url)
    .pipe(
      map(this.extractData),
      
      
      catchError(this.handleError('getVehicles', []))
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
