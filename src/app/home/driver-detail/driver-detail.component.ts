import { Component, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.component.html',
  styleUrls: ['./driver-detail.component.css']
})
export class DriverDetailComponent implements OnInit {

  directionUrl: String;

  constructor() { }

  ngOnInit() {
    this.directionUrl = environment.hostUrl+'/vehicleRequest';
  }

}
