import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';

import { Observable } from 'rxjs';
import { catchError, map, timeout, retry } from 'rxjs/operators';

@Injectable()
export class ValidateService {
  private handleError: HandleError;
  
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('OtpService');
  }

  requestCustomerOTP(url,data){
    return this.http.post(url, data)
    .pipe(
      catchError(this.handleError('requestCustomerOTP', []))
    );
  }

  submitCustomerOTP(url,data){
    return this.http.post(url, data)
    .pipe(
      
      
      catchError(this.handleError('submitCustomerOTP', []))
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
