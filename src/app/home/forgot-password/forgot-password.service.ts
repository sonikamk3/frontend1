import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import { HttpErrorHandler, HandleError } from '../../http-error-handler.service';

import { Observable } from 'rxjs';
import { catchError, map, timeout, retry } from 'rxjs/operators';

@Injectable()
export class ForgotPasswordService {
  private handleError: HandleError;
  
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('ForgotPasswordService');
  }

  resetPassword(url,data){
    return this.http.put(url, data)
    .pipe(
      catchError(this.handleError('resetPassword', []))
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
}
