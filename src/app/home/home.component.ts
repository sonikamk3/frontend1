import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Globalservice } from '../service/globals.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  subscription: Subscription;
  loading: Boolean;

  constructor(private globalservice: Globalservice) { }

  ngOnInit() {
    this.loading = false;
    
    this.subscription = this.globalservice.loadingFlag$
      .subscribe(item =>  setTimeout(() => this.loading = item, 0));
  }

  ngOnDestroy() {
    this.globalservice.setLoadingFlag(false);
    /* prevent memory leak when component is destroyed */
    this.subscription.unsubscribe();
  }

}
