import { Component, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-franchise-detail',
  templateUrl: './franchise-detail.component.html',
  styleUrls: ['./franchise-detail.component.css']
})
export class FranchiseDetailComponent implements OnInit {

  directionUrl: String;

  constructor() { }

  ngOnInit() {
    this.directionUrl = environment.hostUrl+'/franchiseEnquiry';
  }

}
