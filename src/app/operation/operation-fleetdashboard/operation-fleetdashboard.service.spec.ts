import { TestBed } from '@angular/core/testing';

import { OperationFleetdashboardService } from './operation-fleetdashboard.service';

describe('OperationFleetdashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationFleetdashboardService = TestBed.get(OperationFleetdashboardService);
    expect(service).toBeTruthy();
  });
});
