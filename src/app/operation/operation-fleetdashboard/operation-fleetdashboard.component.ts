import { Component, OnInit } from '@angular/core';
import { OperationFleetdashboardService } from './operation-fleetdashboard.service';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-operation-fleetdashboard',
  templateUrl: './operation-fleetdashboard.component.html',
  styleUrls: ['./operation-fleetdashboard.component.css']
})
export class OperationFleetdashboardComponent implements OnInit {
  fleetCount: any;
  activefleetCount: any;
  inactivefleetCount: any;

  constructor(private operationFleetdashboardservice:OperationFleetdashboardService) { }

  ngOnInit() {
    this.operationFleetdashboardservice.getOperationFleetdashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFleet/1132")
    .subscribe(response => {
      this.fleetCount={...response};
      this.fleetCount=this.fleetCount.data;
      console.log(this.fleetCount)
      
    })

    this.operationFleetdashboardservice.getOperationFleetdashboard(AppSettings.TIEUP_ENDPOINT+"viewOTFleetActivate/1132")
    .subscribe(response => {
      this.activefleetCount={...response};
      this.activefleetCount=this.activefleetCount.data[0];
      console.log(this.activefleetCount)
      
    })

    this.operationFleetdashboardservice.getOperationFleetdashboard(AppSettings.TIEUP_ENDPOINT+"viewOTFleetRegistration/1132")
    .subscribe(response => {
      this.inactivefleetCount={...response};
      this.inactivefleetCount=this.inactivefleetCount.data;
      console.log(this.inactivefleetCount)
      
    })
  }

}
