import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFleetdashboardComponent } from './operation-fleetdashboard.component';

describe('OperationFleetdashboardComponent', () => {
  let component: OperationFleetdashboardComponent;
  let fixture: ComponentFixture<OperationFleetdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFleetdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFleetdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
