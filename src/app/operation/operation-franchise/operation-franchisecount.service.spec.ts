import { TestBed } from '@angular/core/testing';

import { OperationFranchisecountService } from './operation-franchisecount.service';

describe('OperationFranchisecountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationFranchisecountService = TestBed.get(OperationFranchisecountService);
    expect(service).toBeTruthy();
  });
});
