import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFranchiseviewComponent } from './operation-franchiseview.component';

describe('OperationFranchiseviewComponent', () => {
  let component: OperationFranchiseviewComponent;
  let fixture: ComponentFixture<OperationFranchiseviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFranchiseviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFranchiseviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
