import { Component, OnInit, ViewChild } from '@angular/core';
import { AppSettings } from 'src/app/constants/constants.module';
import { OperationFranchiselistService } from './operation-franchiselist.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { StorageService } from 'src/app/service/storage.service';
import { Globalservice } from 'src/app/service/globals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operation-franchise',
  templateUrl: './operation-franchise.component.html',
  styleUrls: ['./operation-franchise.component.css']
})
export class OperationFranchiseComponent implements OnInit {
  franchisedisplayColumns: string[]= ['companyName', 'address', 'mobileNumber', 'emailId', 'yearOfContract','Action'];
  franchisedisplayColumns1: string[]= ['role', 'requestNumber', 'name', 'mobileNumber', 'email','message','creationDate','Action'];
  franchisedisplayColumns2: string[]= ['role', 'requestNumber', 'name', 'mobileNumber', 'email','cityName','onHoldReason','creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  
  operationfranchiselistservice: any;
  operationFranchiselist: any;
  tabList=[
    {'id':0, 'label': 'ACTIVE'},
    {'id':1, 'label': 'APPROVAL'},
    {'id':2, 'label': 'FOLLOW-UPS'},
    {'id':3, 'label': 'INPROCESS'},
    {'id':4, 'label': 'ON-HOLD'},
  ];
  operationFranchiseactivelist: any;
  operationFranchisefollowuplist: any;
  operationInprocesslist: any;
  operationOnholdlist: any;
  operationFranchiseapprovallist: any;
  operationFranchiseInprocesslist: any;
  OperationFranchiseOnholdlistt: any;
  operationFranchiseOnholdlistt: any;
  operationFranchiseOnholdlist: any;
  tabIndex: any;
  userId: string;
  active: boolean;
  followups: boolean;
  onhold: boolean;
  

  constructor(private operationFranchiselistService:OperationFranchiselistService,private storageService:StorageService, private globalService:Globalservice, private router:Router) { }


  ngOnInit() 
  {
    this.active=true;
    this.userId=this.storageService.getItem('user').data.id
    this.franchiseList(AppSettings.TIEUP_ENDPOINT + 'franchise2/state/' + this.userId);
  }
     
      tabChange(event) {
        
        this.tabIndex = event.index;
        this.getFranchiseList(this.tabIndex, true);
      }
      private getFranchiseList(bookingType, flag) {
        if (flag)
          
        switch (bookingType) {
          case 0:
            this.active=true;
            this.followups=false;
            this.onhold=false;
            this.franchiseList(AppSettings.TIEUP_ENDPOINT + 'franchise2/state/' + this.userId);
            break;
          case 1:
            this.active=true;
            this.followups=false;
            this.onhold=false;
            this.franchiseList(AppSettings.TIEUP_ENDPOINT + 'franchiseList/state/' + this.userId);
            break;
          case 2:
            this.active=false;
            this.followups=true;
            this.onhold=false;
            this.franchiseList(AppSettings.TIEUP_ENDPOINT + 'boardingrequest/status1/state/9/' + this.userId);
            break;
          case 3:
            this.active=false;
            this.followups=true;
            this.onhold=false;
            this.franchiseList(AppSettings.TIEUP_ENDPOINT + 'boardingrequest/status2/state/9/' + this.userId);
            break;
            case 4:
              this.active=false;
            this.followups=false;
            this.onhold=true;
            this.franchiseList(AppSettings.TIEUP_ENDPOINT + 'boardingrequest/status4/state/9/' + this.userId);
          default:
            break;
        }
      }  
      franchiseList(url){
        this.operationFranchiselistService.OperationFranchiseInprocesslist(url)
      .subscribe(response => {
        this.operationFranchiselist={...response};
        this.operationFranchiselist=this.operationFranchiselist.data;
        console.log(this.operationFranchiselist)
        this.pagination();
        })

      }  

      viewFranchiseactive(element){
        this.globalService.getFranchiseactiveId(element);
        this.router.navigate(['/franchiseactiveview']);
      }
      
      private pagination() {
        this.operationFranchiselist = new MatTableDataSource<any>(this.operationFranchiselist);
        this.operationFranchiselist.paginator = this.paginator;
        this.operationFranchiselist.sort = this.sort;
      }
    
      applyFilter(filterValue: string) {
        this.operationFranchiselist.filter = filterValue.trim().toLowerCase();
    
        if (this.operationFranchiselist.paginator) {
          this.operationFranchiselist.paginator.firstPage();
        }
    
    }
    }