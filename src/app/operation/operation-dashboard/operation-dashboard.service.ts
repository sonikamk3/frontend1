import { Injectable } from '@angular/core';
import { HttpErrorHandler, HandleError } from 'src/app/http-error-handler.service';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationDashboardService {
  handlerError: HandleError;

  constructor( 
    private http:HttpClient,
    httpErrorHandler:HttpErrorHandler) {
      this.handlerError = httpErrorHandler.createHandleError('OperationDashboardService')
    }
    
   

getOperationDashboard(url){
  return this.http.get(url)
.pipe(
  map(this.extractData),
  catchError(this.handlerError('getOperationDashboard',[]))
  )}

  private extractData(res: Response){
    let body = res;
    return body || {};
  }
}
