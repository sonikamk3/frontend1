import { Component, OnInit } from '@angular/core';
import { OperationDashboardService } from './operation-dashboard.service';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-operation-dashboard',
  templateUrl: './operation-dashboard.component.html',
  styleUrls: ['./operation-dashboard.component.css']
})
export class OperationDashboardComponent implements OnInit {
  fleetCount: any;
  franchiseCount: any;
  warehouseCount: any;
  totalenquiryCount: any;
  customerCount: any;

  constructor(private operationDashboardservice:OperationDashboardService) { }

  ngOnInit() {
    this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFleet/1132")
    .subscribe(response => {
      this.fleetCount={...response};
      this.fleetCount=this.fleetCount.data;
      console.log(this.fleetCount)
      
    })
    this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTFranchise/1132")
    .subscribe(response=> {
      this.franchiseCount={...response};
      this.franchiseCount=this.franchiseCount.data;
      console.log(this.franchiseCount)
    })
    this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTWarehouse/1132")
    .subscribe(response=> {
      this.warehouseCount={...response};
      this.warehouseCount=this.warehouseCount.data;
      console.log(this.franchiseCount)
    })
    this.operationDashboardservice.getOperationDashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTBoardingRequests/1132")
    .subscribe(response=> {
      this.totalenquiryCount={...response};
      this.totalenquiryCount=this.totalenquiryCount.data;
      console.log(this.totalenquiryCount)
    })
   this.operationDashboardservice.getOperationDashboard(AppSettings.COMMON_ENDPOINT+"viewDashboardcustomer?")
   .subscribe(response=> {
     this.customerCount={...response};
     this.customerCount=this.customerCount.data[0];
     console.log(this.customerCount)
   })
  }


}
