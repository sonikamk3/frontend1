import { Component, OnInit } from '@angular/core';
import { OperationEnquirydashboardService } from './operation-enquirydashboard.service';
import { AppSettings } from 'src/app/constants/constants.module';

@Component({
  selector: 'app-operation-enquirydashboard',
  templateUrl: './operation-enquirydashboard.component.html',
  styleUrls: ['./operation-enquirydashboard.component.css']
})
export class OperationEnquirydashboardComponent implements OnInit {
  franchiseCount: any;
  fleetenquiryCount: any;
  warehouseenquiryCount: any;
  driverCount: any;
  vehicleCount: any;
  labourCount: any;
  enterpriseCount: any;

  constructor(private operationEnquirydashboardService:OperationEnquirydashboardService) { }

  ngOnInit() {
    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"totalOTOnboardFranchise/1132")
    .subscribe(response => {
      this.franchiseCount={...response};
      this.franchiseCount=this.franchiseCount.data[0];
      console.log(this.franchiseCount)  
    })

    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"totalOTOnboardFleet/1132")
    .subscribe(response => {
      this.fleetenquiryCount={...response};
      this.fleetenquiryCount=this.fleetenquiryCount.data[0];
      console.log(this.fleetenquiryCount)
    })

    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"totalOTOnboardWarehouse/1132")
    .subscribe(response => {
      this.warehouseenquiryCount={...response};
      this.warehouseenquiryCount=this.warehouseenquiryCount.data[0];
      console.log(this.warehouseenquiryCount)
    })

    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"getOTDriverCount/1132")
    .subscribe(response => {
      this.driverCount={...response};
      this.driverCount=this.driverCount.data;
      console.log(this.driverCount)
    })

    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"onboardVehicleCount/1132")
    .subscribe(response => {
      this.vehicleCount={...response};
      this.vehicleCount=this.vehicleCount.data;
      console.log(this.vehicleCount)
    })

    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"onboardLabourCount/1132")
    .subscribe(response => {
      this.labourCount={...response};
      this.labourCount=this.labourCount.data;
      console.log(this.labourCount)
    })

    this.operationEnquirydashboardService.getOperationEnquirydashboard(AppSettings.TIEUP_ENDPOINT+"getTotalOTOnboardEnterprise/1132")
    .subscribe(response => {
      this.enterpriseCount={...response};
      this.enterpriseCount=this.enterpriseCount.data;
      console.log(this.enterpriseCount)
    })
    
    }
  }
