import { TestBed } from '@angular/core/testing';

import { OperationEnquirydashboardService } from './operation-enquirydashboard.service';

describe('OperationEnquirydashboardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationEnquirydashboardService = TestBed.get(OperationEnquirydashboardService);
    expect(service).toBeTruthy();
  });
});
