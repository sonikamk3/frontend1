import { TestBed } from '@angular/core/testing';

import { OperationWarehouseenquiryService } from './operation-warehouseenquiry.service';

describe('OperationWarehouseenquiryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationWarehouseenquiryService = TestBed.get(OperationWarehouseenquiryService);
    expect(service).toBeTruthy();
  });
});
