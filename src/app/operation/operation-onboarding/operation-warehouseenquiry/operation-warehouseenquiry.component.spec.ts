import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationWarehouseenquiryComponent } from './operation-warehouseenquiry.component';

describe('OperationWarehouseenquiryComponent', () => {
  let component: OperationWarehouseenquiryComponent;
  let fixture: ComponentFixture<OperationWarehouseenquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationWarehouseenquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationWarehouseenquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
