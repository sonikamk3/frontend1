import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationWarehouseenquiryService } from './operation-warehouseenquiry.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-operation-warehouseenquiry',
  templateUrl: './operation-warehouseenquiry.component.html',
  styleUrls: ['./operation-warehouseenquiry.component.css']
})
export class OperationWarehouseenquiryComponent implements OnInit {
  warehouseenquirydisplayColumns: string[]= ['requestNumber', 'name', 'mobileNumber', 'email', 'message','creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationWarehouseenquirylist: any;

  constructor(private operationWarehouseenquiryService:OperationWarehouseenquiryService) { }

  ngOnInit() {
    this.operationWarehouseenquiryService.OperationWarehouseenquiryService(AppSettings.TIEUP_ENDPOINT+"opsboardingrequest/4/1132")
      .subscribe(response => {
        this.operationWarehouseenquirylist={...response};
        this.operationWarehouseenquirylist=this.operationWarehouseenquirylist.data;
        console.log(this.operationWarehouseenquirylist)
        this.pagination();
      });
  }
  private pagination() {
    this.operationWarehouseenquirylist = new MatTableDataSource<any>(this.operationWarehouseenquirylist);
    this.operationWarehouseenquirylist.paginator = this.paginator;
    this.operationWarehouseenquirylist.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationWarehouseenquirylist.filter = filterValue.trim().toLowerCase();

    if (this.operationWarehouseenquirylist.paginator) {
      this.operationWarehouseenquirylist.paginator.firstPage();
    }
  }

}
