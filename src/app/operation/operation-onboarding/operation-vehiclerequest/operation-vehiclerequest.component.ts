import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationVehiclerequestService } from './operation-vehiclerequest.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Globalservice } from 'src/app/service/globals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operation-vehiclerequest',
  templateUrl: './operation-vehiclerequest.component.html',
  styleUrls: ['./operation-vehiclerequest.component.css']
})
export class OperationVehiclerequestComponent implements OnInit {
  vehiclerequestdisplayColumns: string[]= ['vehicleId', 'registrationNumber', 'ownerName', 'category', 'creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationVehiclerequestlist: any;

  constructor(private operationVehiclerequestService:OperationVehiclerequestService,private globalService:Globalservice,private router:Router) { }

  ngOnInit() {
    this.operationVehiclerequestService.OperationVehiclerequesttlist(AppSettings.TIEUP_ENDPOINT+"onboardstatevehiclelist/1132")
      .subscribe(response => {
        this.operationVehiclerequestlist={...response};
        this.operationVehiclerequestlist=this.operationVehiclerequestlist.data;
        console.log(this.operationVehiclerequestlist)
        this.pagination();
      });
  }
  viewVehicle(element){
    this.globalService.getVehicleId(element);
    this.router.navigate(['/vehicleView']);

  }
  
  private pagination() {
    this.operationVehiclerequestlist = new MatTableDataSource<any>(this.operationVehiclerequestlist);
    this.operationVehiclerequestlist.paginator = this.paginator;
    this.operationVehiclerequestlist.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationVehiclerequestlist.filter = filterValue.trim().toLowerCase();

    if (this.operationVehiclerequestlist.paginator) {
      this.operationVehiclerequestlist.paginator.firstPage();
    }

}

}
