import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFleetenquiryComponent } from './operation-fleetenquiry.component';

describe('OperationFleetenquiryComponent', () => {
  let component: OperationFleetenquiryComponent;
  let fixture: ComponentFixture<OperationFleetenquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFleetenquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFleetenquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
