import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationLabourrequestComponent } from './operation-labourrequest.component';

describe('OperationLabourrequestComponent', () => {
  let component: OperationLabourrequestComponent;
  let fixture: ComponentFixture<OperationLabourrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationLabourrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationLabourrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
