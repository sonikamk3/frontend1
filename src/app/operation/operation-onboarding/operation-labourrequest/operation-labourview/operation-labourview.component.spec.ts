import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationLabourviewComponent } from './operation-labourview.component';

describe('OperationLabourviewComponent', () => {
  let component: OperationLabourviewComponent;
  let fixture: ComponentFixture<OperationLabourviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationLabourviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationLabourviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
