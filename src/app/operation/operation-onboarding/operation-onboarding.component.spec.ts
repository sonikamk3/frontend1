import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationOnboardingComponent } from './operation-onboarding.component';

describe('OperationOnboardingComponent', () => {
  let component: OperationOnboardingComponent;
  let fixture: ComponentFixture<OperationOnboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationOnboardingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationOnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
