import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationEnterpriserequestComponent } from './operation-enterpriserequest.component';

describe('OperationEnterpriserequestComponent', () => {
  let component: OperationEnterpriserequestComponent;
  let fixture: ComponentFixture<OperationEnterpriserequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationEnterpriserequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationEnterpriserequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
