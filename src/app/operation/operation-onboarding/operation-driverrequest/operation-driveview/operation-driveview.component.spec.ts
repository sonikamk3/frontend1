import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationDriveviewComponent } from './operation-driveview.component';

describe('OperationDriveviewComponent', () => {
  let component: OperationDriveviewComponent;
  let fixture: ComponentFixture<OperationDriveviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationDriveviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationDriveviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
