import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationDriverrequestService } from './operation-driverrequest.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Globalservice } from 'src/app/service/globals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operation-driverrequest',
  templateUrl: './operation-driverrequest.component.html',
  styleUrls: ['./operation-driverrequest.component.css']
})
export class OperationDriverrequestComponent implements OnInit {
  driverrequestdisplayColumns: string[]= ['driverId', 'firstName', 'mobileNumber', 'emailId', 'creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationDriverrequestlist: any;
  
  
  constructor(private operationDriverrequestService:OperationDriverrequestService,private globalService:Globalservice,private router:Router ) { }

  ngOnInit() {
    this.operationDriverrequestService.OperationDriverrequestlist(AppSettings.TIEUP_ENDPOINT+"onboardstatedriverlist/1132?")
      .subscribe(response => {
        this.operationDriverrequestlist={...response};
        this.operationDriverrequestlist=this.operationDriverrequestlist.data;
        console.log(this.operationDriverrequestlist)
        this.pagination();
      });
    
  }
  viewDriver(element){
    this.globalService.getDriverId(element);
    this.router.navigate(['/driverView']);

  }
  
  private pagination() {
    this.operationDriverrequestlist = new MatTableDataSource<any>(this.operationDriverrequestlist);
    this.operationDriverrequestlist.paginator = this.paginator;
    this.operationDriverrequestlist.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationDriverrequestlist.filter = filterValue.trim().toLowerCase();

    if (this.operationDriverrequestlist.paginator) {
      this.operationDriverrequestlist.paginator.firstPage();
    }

}

}
