import { TestBed } from '@angular/core/testing';

import { OperationDriverrequestService } from './operation-driverrequest.service';

describe('OperationDriverrequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationDriverrequestService = TestBed.get(OperationDriverrequestService);
    expect(service).toBeTruthy();
  });
});
