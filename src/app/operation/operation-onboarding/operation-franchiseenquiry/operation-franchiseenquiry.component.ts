import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationFranchiseenquiryService } from './operation-franchiseenquiry.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Globalservice } from 'src/app/service/globals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operation-franchiseenquiry',
  templateUrl: './operation-franchiseenquiry.component.html',
  styleUrls: ['./operation-franchiseenquiry.component.css']
})
export class OperationFranchiseenquiryComponent implements OnInit {
  franchiseenquirydisplayColumns: string[]= ['requestNumber', 'name', 'mobileNumber', 'email', 'message','creationDate','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationFranchiselist: any;
  

  constructor(private operationFranchiseenquiryService:OperationFranchiseenquiryService,private globalService:Globalservice,private router:Router) { }

  ngOnInit() {
    this.operationFranchiseenquiryService.OperationFranchiseenquiryService(AppSettings.TIEUP_ENDPOINT+"getRequestStateListCSVDetails/9/1132")
      .subscribe(response => {
        this.operationFranchiselist={...response};
        this.operationFranchiselist=this.operationFranchiselist.data;
        console.log(this.operationFranchiselist)
        this.pagination();
      });
  }
  viewFranchise(element){
    this.globalService.getFranchiseId(element);
    this.router.navigate(['/franchiseView']);

  }
  private pagination() {
    this.operationFranchiselist = new MatTableDataSource<any>(this.operationFranchiselist);
    this.operationFranchiselist.paginator = this.paginator;
    this.operationFranchiselist.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationFranchiselist.filter = filterValue.trim().toLowerCase();

    if (this.operationFranchiselist.paginator) {
      this.operationFranchiselist.paginator.firstPage();
    }
  }

}
