import { TestBed } from '@angular/core/testing';

import { OperationFranchiseenquiryService } from './operation-franchiseenquiry.service';

describe('OperationFranchiseenquiryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationFranchiseenquiryService = TestBed.get(OperationFranchiseenquiryService);
    expect(service).toBeTruthy();
  });
});
