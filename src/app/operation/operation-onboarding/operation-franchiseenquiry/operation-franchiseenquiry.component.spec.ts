import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFranchiseenquiryComponent } from './operation-franchiseenquiry.component';

describe('OperationFranchiseenquiryComponent', () => {
  let component: OperationFranchiseenquiryComponent;
  let fixture: ComponentFixture<OperationFranchiseenquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFranchiseenquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFranchiseenquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
