import { TestBed } from '@angular/core/testing';

import { OperationCustomerviewService } from './operation-customerview.service';

describe('OperationCustomerviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationCustomerviewService = TestBed.get(OperationCustomerviewService);
    expect(service).toBeTruthy();
  });
});
