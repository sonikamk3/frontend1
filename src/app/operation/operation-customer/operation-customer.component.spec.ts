import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationCustomerComponent } from './operation-customer.component';

describe('OperationCustomerComponent', () => {
  let component: OperationCustomerComponent;
  let fixture: ComponentFixture<OperationCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
