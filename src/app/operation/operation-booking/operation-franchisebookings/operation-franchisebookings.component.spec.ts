import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFranchisebookingsComponent } from './operation-franchisebookings.component';

describe('OperationFranchisebookingsComponent', () => {
  let component: OperationFranchisebookingsComponent;
  let fixture: ComponentFixture<OperationFranchisebookingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFranchisebookingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFranchisebookingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
