import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationOfflinebookingService } from './operation-offlinebooking.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-operation-offlinebooking',
  templateUrl: './operation-offlinebooking.component.html',
  styleUrls: ['./operation-offlinebooking.component.css']
})
export class OperationOfflinebookingComponent implements OnInit {
  offlinebookingdisplayColumns: string[]= ['bookingId', 'source', 'destination', 'goodsType', 'goodsWeight','totalAmount','Assign'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  operationOfflinebookinglist: any;

  

  constructor(private operationOfflinebookingService:OperationOfflinebookingService) { }

  ngOnInit() {
    this.operationOfflinebookingService.OperationOfflineBookinglist(AppSettings.CUSTOMER_ENDPOINT+"offline/Pending/booking/1750")
      .subscribe(response => {
        this.operationOfflinebookinglist={...response};
        this.operationOfflinebookinglist=this.operationOfflinebookinglist.data;
        console.log(this.operationOfflinebookinglist)
        this.pagination();
      });
  }
  private pagination() {
    this.operationOfflinebookinglist = new MatTableDataSource<any>(this.operationOfflinebookinglist);
    this.operationOfflinebookinglist.paginator = this.paginator;
    this.operationOfflinebookinglist.sort = this.sort;
  }

}
