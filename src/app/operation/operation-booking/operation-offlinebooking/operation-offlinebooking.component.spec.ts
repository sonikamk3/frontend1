import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationOfflinebookingComponent } from './operation-offlinebooking.component';

describe('OperationOfflinebookingComponent', () => {
  let component: OperationOfflinebookingComponent;
  let fixture: ComponentFixture<OperationOfflinebookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationOfflinebookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationOfflinebookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
