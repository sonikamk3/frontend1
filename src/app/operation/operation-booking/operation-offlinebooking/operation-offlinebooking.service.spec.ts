import { TestBed } from '@angular/core/testing';

import { OperationOfflinebookingService } from './operation-offlinebooking.service';

describe('OperationOfflinebookingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OperationOfflinebookingService = TestBed.get(OperationOfflinebookingService);
    expect(service).toBeTruthy();
  });
});
