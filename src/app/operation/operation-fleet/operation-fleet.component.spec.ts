import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationFleetComponent } from './operation-fleet.component';

describe('OperationFleetComponent', () => {
  let component: OperationFleetComponent;
  let fixture: ComponentFixture<OperationFleetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationFleetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationFleetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
