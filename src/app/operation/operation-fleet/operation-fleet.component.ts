import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationFleetService } from './operation-fleet.service';
import { AppSettings } from 'src/app/constants/constants.module';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-operation-fleet',
  templateUrl: './operation-fleet.component.html',
  styleUrls: ['./operation-fleet.component.css']
})
export class OperationFleetComponent implements OnInit {
  fleetdisplayColumns: string[]= ['fleetId', 'companyName', 'mobileNumber', 'emailId', 'yearOfContract','Action'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  tabList=[
    {'id':0, 'label': 'ACTIVE'},
    {'id':1, 'label': 'APPROVAL'},
    {'id':2, 'label': 'FOLLOW-UPS'},
    {'id':3, 'label': 'INPROCESS'},
    {'id':4, 'label': 'ON-HOLD'},
  ];
  operationFleetlist: any;
  operationactiveFleetlist: any;
  operationapprovalFleetlist: any;
  operationfollowupFleetlist: any;
  operationinprocessFleetlist: any;
  operationonholdFleetlist: any;

  constructor(private operationFleetService:OperationFleetService) { }

  ngOnInit() {

    this.operationFleetService.OperationFleetlist(AppSettings.TIEUP_ENDPOINT+"fleetOperator2/state/1132")
      .subscribe(response => {
        this.operationFleetlist={...response};
        this.operationFleetlist=this.operationFleetlist.data;
        console.log(this.operationFleetlist)
        this.pagination();
      });

      this.operationFleetService.OperationFleetlist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status1/state/3/1132")
      .subscribe(response => {
        this.operationactiveFleetlist={...response};
        this.operationactiveFleetlist=this.operationactiveFleetlist.data;
        console.log(this.operationactiveFleetlist)
      });

      this.operationFleetService.OperationFleetlist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status2/state/3/1132")
      .subscribe(response => {
        this.operationapprovalFleetlist={...response};
        this.operationapprovalFleetlist=this.operationapprovalFleetlist.data;
        console.log(this.operationapprovalFleetlist)
      });

      this.operationFleetService.OperationFleetlist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status4/state/3/1132")
      .subscribe(response => {
        this.operationfollowupFleetlist={...response};
        this.operationfollowupFleetlist=this.operationfollowupFleetlist.data;
        console.log(this.operationfollowupFleetlist)
      });

      this.operationFleetService.OperationFleetlist(AppSettings.TIEUP_ENDPOINT+"fleetOperator2/state/1132")
      .subscribe(response => {
        this.operationinprocessFleetlist={...response};
        this.operationinprocessFleetlist=this.operationinprocessFleetlist.data;
        console.log(this.operationinprocessFleetlist)
      });

      this.operationFleetService.OperationFleetlist(AppSettings.TIEUP_ENDPOINT+"boardingrequest/status1/state/3/1132")
      .subscribe(response => {
        this.operationonholdFleetlist={...response};
        this.operationonholdFleetlist=this.operationonholdFleetlist.data;
        console.log(this.operationonholdFleetlist)
      });
  }
  private pagination() {
    this.operationFleetlist = new MatTableDataSource<any>(this.operationFleetlist);
    this.operationFleetlist.paginator = this.paginator;
    this.operationFleetlist.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.operationFleetlist.filter = filterValue.trim().toLowerCase();

    if (this.operationFleetlist.paginator) {
      this.operationFleetlist.paginator.firstPage();
    }

}

}
