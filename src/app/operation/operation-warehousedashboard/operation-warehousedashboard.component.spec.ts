import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationWarehousedashboardComponent } from './operation-warehousedashboard.component';

describe('OperationWarehousedashboardComponent', () => {
  let component: OperationWarehousedashboardComponent;
  let fixture: ComponentFixture<OperationWarehousedashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationWarehousedashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationWarehousedashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
