import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorHandler } from 'src/app/http-error-handler.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationWarehouseService {
  
  handlerError: any;

  constructor(
    private http:HttpClient,
    httpErrorhandler:HttpErrorHandler) {
      this.handlerError = httpErrorhandler.createHandleError('OperationWarehouseService') 
     }
     OperationWarehouselist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationWarehouselist',[]))
      )
    }
    OperationWarehouseactivelist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationWarehouseactivelist',[]))
      )
    }
    OperationWarehouseapprovallist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationWarehouseapprovallist',[]))
      )
    }
    OperationWarehouseFollowupslist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationWarehouseFollowupslist',[]))
      )
    }
    OperationWarehouseInprocesslist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationWarehouseInprocess',[]))
      )
    }
    OperationWarehouseOnholdlist(url){
      return this.http.get(url)
      .pipe(
        map(this.extractData),
        catchError(this.handlerError('OperationWarehouseOnholdlist',[]))
      )
    }

    private extractData(res: Response){
      let body = res;
      return body || {};
}
}
