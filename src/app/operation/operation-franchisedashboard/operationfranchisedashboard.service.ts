import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorHandler } from 'src/app/http-error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class OperationfranchisedashboardService {
  handlerError: any;

  constructor(
    private http:HttpClient,
    httpErrorHandler:HttpErrorHandler) {
      this.handlerError = httpErrorHandler.createHandleError('OperationFranchisedashboardService')
   }

   getOperationFranchisedashboard(url){
    return this.http.get(url)
  .pipe(
    map(this.extractData),
    catchError(this.handlerError('getOperationFranchisedashboard',[]))
    )}
  
    private extractData(res: Response){
      let body = res;
      return body || {};
    }
  }