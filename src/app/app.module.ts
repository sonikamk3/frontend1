import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { } from 'googlemaps';
import { DemoMaterialModule } from '../material-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule, GoogleMapsAPIWrapper} from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HomeNavComponent } from './home/home-nav/home-nav.component';
import { HomeFooterComponent } from './home/home-footer/home-footer.component';
import { PrivacyPolicyComponent } from './home/privacy-policy/privacy-policy.component';
import { DisclaimerComponent } from './home/disclaimer/disclaimer.component';
import { FaqsComponent } from './home/faqs/faqs.component';
import { MmgComponent } from './home/mmg/mmg.component';
import { LoginComponent } from './home/login/login.component';
import { DriverDetailComponent } from './home/driver-detail/driver-detail.component';
import { FranchiseDetailComponent } from './home/franchise-detail/franchise-detail.component';
import { ForgotPasswordComponent } from './home/forgot-password/forgot-password.component';
import { ValidateComponent } from './home/validate/validate.component';

import { HomeRouteGuard } from './guard/home-guard.module';
import { DriverRouteGuard } from './guard/driver-guard.module';
import { FranchiseRouteGuard } from './guard/franchise-guard.module';
import { CommonRouteGuard } from './guard/common-guard.module';

import { Globalservice } from './service/globals.service';
import { SnackBarService } from './service/snackbar.service';
import { StorageService } from './service/storage.service';
import { UploadService } from './service/upload.service';
import { MessageService } from './message.service';
import { HttpErrorHandler } from './http-error-handler.service';
import { httpInterceptorProviders } from './http-interceptors/index';

import { UserFooterComponent } from './user-footer/user-footer.component';
import { PopupGeneralComponent } from './popup-general/popup-general.component';




import { TieUpsComponent } from './tie-ups/tie-ups.component';
import { OperationComponent } from './operation/operation.component';
import { TieupsHeaderComponent } from './tie-ups/tieups-header/tieups-header.component';
import { TieupsDashboardComponent } from './tie-ups/tieups-dashboard/tieups-dashboard.component';
import { OperationHeaderComponent } from './operation/operation-header/operation-header.component'
import { OperationDashboardComponent } from './operation/operation-dashboard/operation-dashboard.component';
import { OperationGuard } from './guard/opearation-gaurd.module';
import { TieupsGuard } from './guard/tieups-gaurd.module';
import { OperationFranchiseComponent } from './operation/operation-franchise/operation-franchise.component';
import { OperationFleetComponent } from './operation/operation-fleet/operation-fleet.component';
import { OperationWarehouseComponent } from './operation/operation-warehouse/operation-warehouse.component';
import { OperationCustomerComponent } from './operation/operation-customer/operation-customer.component';
import { OperationFranchisedashboardComponent } from './operation/operation-franchisedashboard/operation-franchisedashboard.component';
import { OperationWarehousedashboardComponent } from './operation/operation-warehousedashboard/operation-warehousedashboard.component';
import { OperationEnquirydashboardComponent } from './operation/operation-enquirydashboard/operation-enquirydashboard.component';
import { OperationFleetdashboardComponent } from './operation/operation-fleetdashboard/operation-fleetdashboard.component';
import { OperationOnboardingComponent } from './operation/operation-onboarding/operation-onboarding.component';
import { OperationFranchiseenquiryComponent } from './operation/operation-onboarding/operation-franchiseenquiry/operation-franchiseenquiry.component';
import { OperationFleetenquiryComponent } from './operation/operation-onboarding/operation-fleetenquiry/operation-fleetenquiry.component';
import { OperationDriverrequestComponent } from './operation/operation-onboarding/operation-driverrequest/operation-driverrequest.component';
import { OperationVehiclerequestComponent } from './operation/operation-onboarding/operation-vehiclerequest/operation-vehiclerequest.component';
import { OperationLabourrequestComponent } from './operation/operation-onboarding/operation-labourrequest/operation-labourrequest.component';
import { OperationEnterpriserequestComponent } from './operation/operation-onboarding/operation-enterpriserequest/operation-enterpriserequest.component';
import { OperationBookingComponent } from './operation/operation-booking/operation-booking.component';
import { OperationBiddingComponent } from './operation/operation-bidding/operation-bidding.component';
import { OperationWarehouseenquiryComponent } from './operation/operation-onboarding/operation-warehouseenquiry/operation-warehouseenquiry.component';
import { OperationFranchiseViewComponent } from './operation/operation-onboarding/operation-franchiseenquiry/operation-franchise-view/operation-franchise-view.component';
import { OperationDriveviewComponent } from './operation/operation-onboarding/operation-driverrequest/operation-driveview/operation-driveview.component';
import { OperationVehicleviewComponent } from './operation/operation-onboarding/operation-vehiclerequest/operation-vehicleview/operation-vehicleview.component';
import { OperationLabourviewComponent } from './operation/operation-onboarding/operation-labourrequest/operation-labourview/operation-labourview.component';
import { OperationFranchiseviewComponent } from './operation/operation-franchise/operation-franchiseview/operation-franchiseview.component';
import { OperationFranchisebookingsComponent } from './operation/operation-booking/operation-franchisebookings/operation-franchisebookings.component';
import { OperationOfflinebookingComponent } from './operation/operation-booking/operation-offlinebooking/operation-offlinebooking.component';
import { EmployeesComponent } from './home/employees/employees.component';
import { CreateEmployeeComponent } from './home/employees/create-employee/create-employee.component';
import { EmployeeDetailsComponent } from './home/employees/employee-details/employee-details.component';
import { EmployeeListComponent } from './home/employees/employee-list/employee-list.component';
import { UpdateEmployeeComponent } from './home/employees/update-employee/update-employee.component';
import { StudentComponent } from './home/student/student.component';
import { StudentAddComponent } from './home/student/student-add/student-add.component';
import { StudentListComponent } from './home/student/student-list/student-list.component';
import { PopupComponent } from './home/student/popup/popup.component';
import { StudentUpdateComponent } from './home/student/student-update/student-update.component';
import { StudentDetailsComponent } from './home/student/student-details/student-details.component';
import { EmployeeComponent } from './tie-ups/employee/employee.component';
import { EmployeeCreateComponent } from './tie-ups/employee/employee-create/employee-create.component';
import { EmployeeUpdateComponent } from './tie-ups/employee/employee-update/employee-update.component';
import { EmployeeListsComponent } from './tie-ups/employee/employee-lists/employee-lists.component';






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HomeNavComponent,
    HomeFooterComponent,
    PrivacyPolicyComponent,
    DisclaimerComponent,
    FaqsComponent,
    MmgComponent,
    LoginComponent,
    DriverDetailComponent,
    FranchiseDetailComponent,
    ForgotPasswordComponent,
    ValidateComponent,
   
    UserFooterComponent,
    PopupGeneralComponent,
    
    
    

    TieUpsComponent,
    OperationComponent,
    TieupsHeaderComponent,
    TieupsDashboardComponent,
    OperationDashboardComponent,
    OperationHeaderComponent,
    OperationFranchiseComponent,
    OperationFleetComponent,
    OperationWarehouseComponent,
    OperationCustomerComponent,
    OperationFranchisedashboardComponent,
    OperationWarehousedashboardComponent,
    OperationEnquirydashboardComponent,
    OperationFleetdashboardComponent,
    OperationOnboardingComponent,
    OperationFranchiseenquiryComponent,
    OperationFleetenquiryComponent,
    OperationDriverrequestComponent,
    OperationVehiclerequestComponent,
    OperationLabourrequestComponent,
    OperationEnterpriserequestComponent,
    OperationBookingComponent,
    OperationBiddingComponent,
    OperationWarehouseenquiryComponent,
    OperationFranchiseViewComponent,
    OperationDriveviewComponent,
    OperationVehicleviewComponent,
    OperationLabourviewComponent,
    OperationFranchiseviewComponent,
    OperationFranchisebookingsComponent,
    OperationOfflinebookingComponent,
    EmployeesComponent,
    CreateEmployeeComponent,
    EmployeeDetailsComponent,
    EmployeeListComponent,
    UpdateEmployeeComponent,
    StudentComponent,
    StudentAddComponent,
    StudentListComponent,
    PopupComponent,
    StudentUpdateComponent,
    StudentDetailsComponent,
    EmployeeComponent,
    EmployeeCreateComponent,
    EmployeeUpdateComponent,
    EmployeeListsComponent,
    
    
    
    

    
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    NgbModule,
    CarouselModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBOgUi33PPaRjaZFNcYGZ3OCXYga4CG8FE',
      libraries: ["places","imagery","geometry"],
      region: 'IN'
    }),
    AgmDirectionModule,
    DemoMaterialModule,
    BrowserAnimationsModule
  ],
  providers: [
    HttpErrorHandler,
    HomeRouteGuard,
    GoogleMapsAPIWrapper,
    DriverRouteGuard,
    FranchiseRouteGuard,
    CommonRouteGuard,
    OperationGuard,
    TieupsGuard,
    Globalservice,
    SnackBarService,
    StorageService,
    MessageService,
    UploadService,
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent],
  entryComponents: [LoginComponent, PopupGeneralComponent]
})
export class AppModule { }
