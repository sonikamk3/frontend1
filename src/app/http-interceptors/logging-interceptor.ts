import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler,
  HttpRequest, HttpResponse, HttpErrorResponse
} from '@angular/common/http';

import { throwError } from 'rxjs';
import { ErrorCodes } from '../constants/error.codes.module';
import { finalize, tap, catchError, map } from 'rxjs/operators';
import { MessageService } from '../message.service';
import { SnackBarService } from '../service/snackbar.service';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
  constructor(private messenger: MessageService, private snackBarService: SnackBarService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const started = Date.now();
    let ok: string;

    /* extend server response observable with logging */
    return next.handle(req)
      .pipe(
        tap(
          /* Succeeds when there is a response; ignore other events */
          event => ok = event instanceof HttpResponse ? 'succeeded' : '',
          /* Operation failed; error is an HttpErrorResponse */
          error => ok = 'failed'
        ),
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            // console.log('event--->>>', event);
            if (!event.body && event.body.status != "SUCCESS") {
              this.snackBarService.openSnackBar("Unknown Error", null);
              return null;
            }
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          //console.log(error);
          if (error.error instanceof ErrorEvent) {
            /* client-side error */
            errorMessage = `Error: ${error.error.message}`;
          } else {
            /* server-side error */
            //errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            /* If Server doesnt send any error message, Take it from locally in UI side */
            if (error.error.message)
              errorMessage = `Error: ${error.error.message}`;
            else
              errorMessage = ErrorCodes.CODES[error.status];
          }
          return throwError(errorMessage);
        }),
        /* Log when response observable either completes or errors */
        finalize(() => {
          const elapsed = Date.now() - started;
          const msg = `${req.method} "${req.urlWithParams}"
             ${ok} in ${elapsed} ms.`;
          this.messenger.add(msg);
        })
      );
  }
}