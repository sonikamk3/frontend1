import { Component, OnInit, Inject } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatDialogConfig } from "@angular/material";

@Component({
  selector: 'app-popup-general',
  templateUrl: './popup-general.component.html',
  styleUrls: ['./popup-general.component.css']
})
export class PopupGeneralComponent implements OnInit {

  public title: String;

  constructor(private dialogRef: MatDialogRef<PopupGeneralComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any) { }

  ngOnInit() {
    if(this.dialogData)
      this.title = this.dialogData.title;
    else
      this.dialogRef.close();
  }
  close()
  {
    this.dialogRef.close();
  }

  save()
  {
    this.dialogRef.close(this.title);
  }
}
