import { EmployeesService } from './../employees.service';
import { PopupComponent } from './../../../home/student/popup/popup.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Globalservice } from 'src/app/service/globals.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AppSettings } from 'src/app/constants/constants.module';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from './../../../home/employees/employee.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css']
})
export class EmployeeUpdateComponent implements OnInit {

  studentform: FormGroup;
  employeeData: any;
  employees: any;
  favoriteSeason: string;
  seasons: string[] = ['Male', 'Female'];
  errorMsgs = AppSettings.ERROR_MSGS;
  constructor(private formBuilder: FormBuilder, private globalService: Globalservice, private employeesService: EmployeesService, private router: Router, private http: HttpClient, private dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any) {
    this.studentform = this.formBuilder.group({
       id:new FormControl('',[Validators.required]),
      firstName: new FormControl('', [Validators.required, Validators.minLength(2),Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(1),Validators.pattern('[a-zA-Z ]*'),Validators.maxLength(50)]),
      gender:new FormControl('',[Validators.required]),
      phoneNumber:new FormControl('',[Validators.required, Validators.pattern('[6-9][0-9]{9}]*')]),
      email:new FormControl('',[Validators.required]),
      address:new FormControl('',[Validators.required,Validators.minLength(2),Validators.maxLength(250)]),
      panCardNo:new FormControl('',[Validators.required,Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}]*')]),
      
    });
  }
  ngOnInit() {
    this.employeeData = this.globalService.getEmployeeid();
    console.log(this.employeeData);
    if (this.employeeData) {
      this.employeesService. getEmployee(this.employeeData)
        .subscribe(data => {
          console.log(data) //console.log is used to print the errors in console
          this.employees = { ...data };//3 dots are used to fetch only values insted of object
          console.log(data)
          this.employees = this.employees.data;
           console.log(data)

           this.employees.id;
           this.employees.firstName;
           this.employees.lastName;
           this.employees.gender;
           this.employees.phoneNumber;
           this.employees.email;   
          this.employees.address;
          this.employees.panCardNumber;
          delete  this.employees.status; 
          console.log(this.employees);
          this.studentform.setValue(this.employees);
        });
    }

  }
  updateEmployee() {
    let data = this.studentform.value;
    console.log(this.studentform)
    if (this.studentform.invalid) {
      return;
  }
    this.employeesService.updateEmployee(data).subscribe((Response) => {
      alert("updated succesfully")
      this.dialogRef.close();
    });
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.studentform.controls[controlName].hasError(errorName);
  }
  close() {
    this.dialogRef.close();
  }
}

