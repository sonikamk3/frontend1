import { EmployeeService } from './../../../home/employees/employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../employees.service';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  employee: any;
  id: string;

  constructor(private route: ActivatedRoute,private router: Router,
    private employeesService:EmployeesService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    
    
    this.employeesService.getEmployee(this.id)
      .subscribe(data => {
        console.log(data)
        this.employee ={...data};
        this.employee=this.employee.data;
        
      }, error => console.log(error));

  }
  list(){
    this.router.navigate(['employees1']);
  }
}

