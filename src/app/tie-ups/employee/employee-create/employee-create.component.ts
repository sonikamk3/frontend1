import { EmployeesService } from './../employees.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { EmployeeService } from './../../../home/employees/employee.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  studentform: FormGroup;
  favoriteSeason: string;
  // seasons: string[] = ['Male', 'Female'];
  // submitted: false;
  genders = [

    { 'id': 1, 'val': 'Male' },
    { 'id': 2, 'val': 'Female' }
  ];
  // genders = [

  //   { 'id': 1, 'val': 'Male' },
  //   { 'id': 2, 'val': 'Female' }
  // ];


  gender: { id: number; val: string; };
  FormGroup: any;
  

  constructor(private employeesService: EmployeesService,private formBuilder:FormBuilder,private router:Router,private dialogRef: MatDialogRef<EmployeeCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any) {
    this.studentform =this.formBuilder.group({
      // id:new FormControl('',[Validators.required]),
      firstName: new FormControl('', [Validators.required,  Validators.minLength(2), Validators.pattern('[a-zA-Z ]*'), Validators.maxLength(50)]),
      lastName: new FormControl('', [Validators.required,  Validators.minLength(1),Validators.pattern('[a-zA-Z ]*'),Validators.maxLength(50)]),
      gender:new FormControl('',[Validators.required]),
      phoneNumber:new FormControl('',[Validators.required, Validators.pattern('[6-9][0-9]{9}]*')]),
      email:new FormControl('',[Validators.required]),
      address:new FormControl('',[Validators.required,Validators.minLength(2),Validators.maxLength(250)]),
      panCardNo:new FormControl('',[Validators.required,Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}]*')]),
      
    });
   }
  ngOnInit()//it is used to display first when excecute
   {
    this.studentform.controls['gender'].setValue(this.genders[0].val)
  }
  addEmployee(){
   
    let data = this.studentform.value;
    
    console.log(this.studentform)
    // this.FormGroup.disable();
    // this.submitted = true;
    //this.studentform.reset();
    // stop here if form is invalid
    if (this.studentform.invalid) {
        return;
    }
    //  this.studentform.reset();
    this.employeesService.createEmployee(data).subscribe((response) => {
      alert("uploaded succesfully");
      this.dialogRef.close();
      // this.studentform.reset();
      // this.FormGroup.disable();

      //it is used to clear the form after the submit
    //  this.gotoList();
    }
    
  );
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.studentform.controls[controlName].hasError(errorName);
  }
  gotoList() {
    
    this.router.navigate(['/employees1']);
  }
  close()
  {
    this.dialogRef.close();
  }
  gotoList1() {
    
    this.router.navigate(['/studentadd']);
  }
  // disable(){
  //   this.disable();
  // }

}
