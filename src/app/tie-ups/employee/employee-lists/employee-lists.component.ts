import { EmployeeCreateComponent } from './../employee-create/employee-create.component';
import { PopupGeneralComponent } from 'src/app/popup-general/popup-general.component';
import { EmployeeUpdateComponent } from './../employee-update/employee-update.component';
import { MatDialogConfig } from '@angular/material/dialog';
import { StorageService } from 'src/app/service/storage.service';
import { Globalservice } from 'src/app/service/globals.service';
import { EmployeesService } from './../employees.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { PopupComponent } from 'src/app/home/student/popup/popup.component';

@Component({
  selector: 'app-employee-lists',
  templateUrl: './employee-lists.component.html',
  styleUrls: ['./employee-lists.component.css']
})
export class EmployeeListsComponent implements OnInit {

  title: String;
  id:string;
  firstName:string;
  

  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'gender','address','action',];//this is used
  @ViewChild(MatPaginator,{static:false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static:false}) sort: MatSort;
  employees: any;
  // employees1: MatTableDataSource<any>;
  private baseUrl = 'http://localhost:8085/demo/api/v1/employee/';
  private baseUrl2 = 'http://localhost:8085/demo/api/v1/employee/status/';
  grid: any;


  transform(employees: any, searchText: any): any {
   
  }
  
  keys: string[] = ['firstName','lastName','gender','phoneNumber','address'];
  

  constructor(private employeesService: EmployeesService,private globalService: Globalservice, private dialog: MatDialog, private storageService: StorageService,
    private router: Router) { }

    ngOnInit() 
    {
      this.reload();
    }
  private reload() {
   let response;
  
   response =  this.employeesService.getAllEmployee(this.baseUrl);
   response.subscribe((data)=>{
    this.employees={...data};
    this.employees = this.employees.data;
    console.log(this.employees);
    this.pagination();
    });
    }
    deleteEmployee(id: string,firstName:string) {
      const dialogConfig = new MatDialogConfig();
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
      dialogConfig.data = {
        title : 'Are You Sure Want To Delete ?' ,
         id : id ,
        firstName :firstName ,
      }
  
      const dialogRef = this.dialog.open(PopupComponent,
        dialogConfig);
        this.globalService.setEmployeeid(id);

        
  
      dialogRef.afterClosed().subscribe(
        data => {
          if (data == undefined)
            return;
            
            this.employeesService.deleteEmployee(id,)
            
            .subscribe(data => {
              this.reload();
              alert("data is deleted sucessfully");
            },
            error => console.log(error));
  
      });
      
    }
    
 
  updateEmployee(id:string,firstName:string){
    
        const dialogRef=this.dialog.open(EmployeeUpdateComponent);
        this.globalService.setEmployeeid(id);

        dialogRef.afterClosed().subscribe(result=>{
          console.log("update");
          this.reload();
        });
  }
  private pagination() {
    this.employees = new MatTableDataSource<any>(this.employees);
    this.employees.paginator = this.paginator;
    this.employees.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.employees.filter = filterValue.trim().toLowerCase();
    if (this.employees.paginator) {
      this.employees.paginator.firstPage();
    
    }
  }
  
  employeeDetails(id: string){
    this.router.navigate(['details1', id]);
  }
isHide : boolean = true;
  active(element){
   
  
  if(element==true){
      this.isHide=false;
 }
  else{
    this.isHide=true;
  }
  let response;
  response =  this.employeesService.getAllEmployee(this.baseUrl2+"/"+element);
  response.subscribe((data)=>{
   this.employees={...data};
   this.employees = this.employees.data;
   console.log(this.employees);
   this.pagination();
   });
  }
  get(employee){

    let response; 
    this.isHide=true;

    response =  this.employeesService.getAllEmployee(this.baseUrl);
    response.subscribe((data)=>{
     this.employees={...data};
     this.employees = this.employees.data;
     console.log(this.employees);
     this.pagination();
     });

  }
  status:boolean;
  patchEmployee(id: string,status:boolean) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title : 'Are You Sure Want To Active or deactive ?' ,
    }

    const dialogRef = this.dialog.open(PopupGeneralComponent,
      dialogConfig);
      console.log(this.status);
    dialogRef.afterClosed().subscribe(
      data => {
        // console.log(this.status);
        // if (data == undefined)
        // return;
        // console.log(this.status);
        if(status == true){
          status  = false;
        }
        else{
         (status = true) 
            // status  == !status;
          }
          console.log(status);
        //  this.globalservice.setEmployeeid(status);
           

          this.employeesService.patchEmployee(id,status)
          .subscribe(data => {
            // this.reload();
            alert(" status updated sucessfully");
          },
          error => console.log(error));

    });
    dialogRef.afterClosed().subscribe(result=>{
      
      // this.reload();
    });
  }
  addemployee1(id:string,firstName:string){
    const dialogRef=this.dialog.open(EmployeeCreateComponent);

    dialogRef.afterClosed().subscribe(result=>{
      console.log("");
      this.reload();
    });
}

searchEmployee(key:string,value:string){
  console.log(key);
 console.log(value);
 if(value.length == 0){
  this.reload();
}
else{
  this.employeesService.searchEmployee(key,value)
  
  .subscribe(data=>{
    this.employees ={...data};
    this.employees = this.employees.data;
  });
  
}
}
}

