import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TieupsDashboardComponent } from './tieups-dashboard.component';

describe('TieupsDashboardComponent', () => {
  let component: TieupsDashboardComponent;
  let fixture: ComponentFixture<TieupsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TieupsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TieupsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
