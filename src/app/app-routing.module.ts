import { StudentDetailsComponent } from './home/student/student-details/student-details.component';
import { StudentListComponent } from './home/student/student-list/student-list.component';
import { StudentAddComponent } from './home/student/student-add/student-add.component';
import { CreateEmployeeComponent } from './home/employees/create-employee/create-employee.component';
import { EmployeesComponent } from './home/employees/employees.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeRouteGuard } from './guard/home-guard.module';
import { DriverRouteGuard } from './guard/driver-guard.module';
import { FranchiseRouteGuard } from './guard/franchise-guard.module';
import { CommonRouteGuard } from './guard/common-guard.module';

import { MmgComponent } from '../app/home/mmg/mmg.component';
import { PrivacyPolicyComponent } from '../app/home/privacy-policy/privacy-policy.component';
import { DisclaimerComponent } from '../app/home/disclaimer/disclaimer.component';
import { FaqsComponent } from '../app/home/faqs/faqs.component';
import { DriverDetailComponent } from '../app/home/driver-detail/driver-detail.component';
import { FranchiseDetailComponent } from '../app/home/franchise-detail/franchise-detail.component';
import { ForgotPasswordComponent } from '../app/home/forgot-password/forgot-password.component';
import { ValidateComponent } from '../app/home/validate/validate.component';


import { OperationDashboardComponent } from './operation/operation-dashboard/operation-dashboard.component';
import { TieupsDashboardComponent } from './tie-ups/tieups-dashboard/tieups-dashboard.component';
import { OperationGuard } from './guard/opearation-gaurd.module';
import { TieupsGuard } from './guard/tieups-gaurd.module';
import { OperationFranchiseComponent } from './operation/operation-franchise/operation-franchise.component';
import { OperationFleetComponent } from './operation/operation-fleet/operation-fleet.component';
import { OperationWarehouseComponent } from './operation/operation-warehouse/operation-warehouse.component';
import { OperationCustomerComponent } from './operation/operation-customer/operation-customer.component';
import { OperationFranchisedashboardComponent } from './operation/operation-franchisedashboard/operation-franchisedashboard.component';
import { OperationEnquirydashboardComponent } from './operation/operation-enquirydashboard/operation-enquirydashboard.component';
import { OperationWarehousedashboardComponent } from './operation/operation-warehousedashboard/operation-warehousedashboard.component';
import { OperationFleetdashboardComponent } from './operation/operation-fleetdashboard/operation-fleetdashboard.component';
import { OperationFranchiseenquiryComponent } from './operation/operation-onboarding/operation-franchiseenquiry/operation-franchiseenquiry.component';
import { OperationFleetenquiryComponent } from './operation/operation-onboarding/operation-fleetenquiry/operation-fleetenquiry.component';
import { OperationVehiclerequestComponent } from './operation/operation-onboarding/operation-vehiclerequest/operation-vehiclerequest.component';
import { OperationDriverrequestComponent } from './operation/operation-onboarding/operation-driverrequest/operation-driverrequest.component';
import { OperationLabourrequestComponent } from './operation/operation-onboarding/operation-labourrequest/operation-labourrequest.component';
import { OperationEnterpriserequestComponent } from './operation/operation-onboarding/operation-enterpriserequest/operation-enterpriserequest.component';
import { OperationBookingComponent } from './operation/operation-booking/operation-booking.component';
import { OperationBiddingComponent } from './operation/operation-bidding/operation-bidding.component';
import { OperationWarehouseenquiryComponent } from './operation/operation-onboarding/operation-warehouseenquiry/operation-warehouseenquiry.component';
import { OperationFranchiseViewComponent } from './operation/operation-onboarding/operation-franchiseenquiry/operation-franchise-view/operation-franchise-view.component';
import { OperationDriveviewComponent } from './operation/operation-onboarding/operation-driverrequest/operation-driveview/operation-driveview.component';
import { OperationVehicleviewComponent } from './operation/operation-onboarding/operation-vehiclerequest/operation-vehicleview/operation-vehicleview.component';
import { OperationLabourviewComponent } from './operation/operation-onboarding/operation-labourrequest/operation-labourview/operation-labourview.component';
import { OperationFranchiseviewComponent } from './operation/operation-franchise/operation-franchiseview/operation-franchiseview.component';
import { OperationOfflinebookingComponent } from './operation/operation-booking/operation-offlinebooking/operation-offlinebooking.component';
import { OperationFranchisebookingsComponent } from './operation/operation-booking/operation-franchisebookings/operation-franchisebookings.component';
import { EmployeeListComponent } from './home/employees/employee-list/employee-list.component';
import { UpdateEmployeeComponent } from './home/employees/update-employee/update-employee.component';
import { EmployeeDetailsComponent } from './home/employees/employee-details/employee-details.component';
import { PopupComponent } from './home/student/popup/popup.component';
import { StudentUpdateComponent } from './home/student/student-update/student-update.component';







const routes: Routes = [
  { path: 'mmg', component: MmgComponent, canActivate: [HomeRouteGuard] },
  { path: 'policy', component: PrivacyPolicyComponent, canActivate: [HomeRouteGuard] },
  { path: 'disclaimer', component: DisclaimerComponent, canActivate: [HomeRouteGuard] },
  { path: 'faqs', component: FaqsComponent, canActivate: [HomeRouteGuard] },
  { path: 'vehicle', component: DriverDetailComponent, canActivate: [HomeRouteGuard] },
  { path: 'franchise', component: FranchiseDetailComponent, canActivate: [HomeRouteGuard] },
  { path: 'forgotPassword', component: ForgotPasswordComponent, canActivate: [HomeRouteGuard] },
  { path: 'validate', component: ValidateComponent, canActivate: [HomeRouteGuard] },
  { path: 'employees',component: EmployeesComponent },
  { path: 'student',component: EmployeesComponent },


 
  { path: 'operationalTeamDashboard', component: OperationDashboardComponent},
  {path: 'operationalFranchise', component: OperationFranchiseComponent},
  {path: 'operationFleet', component: OperationFleetComponent},
  {path: 'operationWarehouse', component: OperationWarehouseComponent},
  {path: 'operationCustomer', component: OperationCustomerComponent},
  {path: 'operationFracnhisedashboard', component: OperationFranchisedashboardComponent},
  {path: 'operationFleetdashboard', component: OperationFleetdashboardComponent},
  {path: 'operationWarehousedashboard', component: OperationWarehousedashboardComponent, canActivate: [OperationGuard] },
  {path: 'operationEnquirydashboard', component: OperationEnquirydashboardComponent, canActivate: [OperationGuard] },
  {path: 'operationFranchiseenquiry', component: OperationFranchiseenquiryComponent, canActivate: [OperationGuard] },
  {path: 'operationFleetenquiry', component: OperationFleetenquiryComponent, canActivate: [OperationGuard] },
  {path: 'operationWarehouseenquiry', component: OperationWarehouseenquiryComponent, canActivate: [OperationGuard] },
  {path: 'operationDriverrequest', component: OperationDriverrequestComponent, canActivate: [OperationGuard] },
  {path: 'operationVehiclerequest', component: OperationVehiclerequestComponent, canActivate: [OperationGuard] },
  {path: 'operationLabourrequest', component: OperationLabourrequestComponent, canActivate: [OperationGuard] },
  {path: 'operationEnterpriserequest', component: OperationEnterpriserequestComponent, canActivate: [OperationGuard] },
  {path: 'operationBooking', component: OperationBookingComponent, canActivate: [OperationGuard] },
  {path: 'operationBidding', component: OperationBiddingComponent, canActivate: [OperationGuard] },
  {path: 'franchiseView', component: OperationFranchiseViewComponent, canActivate: [OperationGuard] },
  {path: 'driverView', component: OperationDriveviewComponent, canActivate: [OperationGuard] },
  {path: 'vehicleView', component: OperationVehicleviewComponent, canActivate: [OperationGuard] },
  {path: 'labourView', component: OperationLabourviewComponent, canActivate: [OperationGuard] },
  {path: 'franchiseactiveview', component: OperationFranchiseviewComponent, canActivate: [OperationGuard] },
  {path: 'franchiseBooking', component: OperationFranchisebookingsComponent, canActivate: [OperationGuard] },
  {path: 'offlineBooking', component: OperationOfflinebookingComponent, canActivate: [OperationGuard] },

  // { path: '', redirectTo: 'employee', pathMatch: 'full' },
  { path: 'employees', component: EmployeeListComponent, canActivate: [TieupsGuard] },
  { path: 'add', component: CreateEmployeeComponent, canActivate: [TieupsGuard]},
  { path: 'update/:id', component: UpdateEmployeeComponent, canActivate: [TieupsGuard]},
  { path: 'details/:id', component: EmployeeDetailsComponent, canActivate: [TieupsGuard]},
  
  { path: 'studentadd', component: StudentAddComponent, canActivate: [TieupsGuard] },
  { path: 'studentlist', component: StudentListComponent},
  { path: 'fgfgfdg',component:PopupComponent},
  { path: 'fgfgfd',component:StudentUpdateComponent},
  { path: 'employees1', component: StudentListComponent },
  { path: 'details1/:id', component: StudentDetailsComponent},
  // { path: 'register', component: StudentUpdateComponent},
  { path: 'employees', component: StudentListComponent },
  
  
  { path: 'tieupsDashboard', component: TieupsDashboardComponent },
  { path: '', component: MmgComponent, canActivate: [HomeRouteGuard] },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
